package com.bus.booking.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RootUriTemplateHandler;
import org.springframework.stereotype.Service;

import com.bus.booking.dao.RoutesStationLinkRepository;
import com.bus.booking.pojos.Route;
import com.bus.booking.pojos.RouteStationLink;
import com.bus.booking.pojos.Station;

@Service
public class BusService {
	
	@Autowired
	RoutesStationLinkRepository routeStationRepo;
	
	public List<Route> getRoutesFor(Station stationA, Station stationB) {
		List<RouteStationLink> routesWithA = this.routeStationRepo.findByStationName(stationA.getName());
		List<RouteStationLink> routesWithB = this.routeStationRepo.findByStationName(stationB.getName());
		
		List<Route> routes = new ArrayList<>();
		
		for (RouteStationLink routeLinkA : routesWithA) {
			Route route = routeLinkA.getRoute();
			
			for (RouteStationLink routeLinkB : routesWithB) {
				if (routeLinkB.getRoute().getName().equals(route.getName())) {
					routes.add(route);
					break;
				}
			}
		}
		
		return routes;
	}
}
