package com.bus.booking.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BusController {

	@GetMapping("/")
	public String getIndexHtml() {
		return "index";
	}
}
