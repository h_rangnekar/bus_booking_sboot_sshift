package com.bus.booking.dao;

import org.springframework.data.repository.CrudRepository;

import com.bus.booking.pojos.Bus;

public interface BusRepository extends CrudRepository<Bus, Integer> {

}
