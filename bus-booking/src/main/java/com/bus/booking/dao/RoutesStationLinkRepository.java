package com.bus.booking.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.bus.booking.pojos.RouteStationLink;

public interface RoutesStationLinkRepository extends CrudRepository<RouteStationLink, Integer>{

	List<RouteStationLink> findByStationName(String name);
}
