package com.bus.booking.pojos;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="route")
public class Route {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="id")
	Integer id;
	
	@Column(name="name")
	String name;
	
	@OneToMany(mappedBy="route")
	List<RouteStationLink> routeStationLinks;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<RouteStationLink> getRouteStationLinks() {
		return routeStationLinks;
	}

	public void setRouteStationLinks(List<RouteStationLink> routeStationLinks) {
		this.routeStationLinks = routeStationLinks;
	}
}
