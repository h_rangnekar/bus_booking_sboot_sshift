package com.bus.booking.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="seat")
public class Seat {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="id")
	Integer id;
	
	@Column(name="name")
	String name;
	
	@ManyToOne
	@JoinColumn(name="bus_id")
	Bus bus;
}
