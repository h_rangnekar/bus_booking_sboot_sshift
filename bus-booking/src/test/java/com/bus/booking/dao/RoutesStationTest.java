package com.bus.booking.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Isolation;

import com.bus.booking.BusBookingSystemApplication;
import com.bus.booking.pojos.Route;
import com.bus.booking.pojos.RouteStationLink;
import com.bus.booking.pojos.Station;
import com.bus.booking.services.BusService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={BusBookingSystemApplication.class, JPATestConfig.class})
@ActiveProfiles("test")
public class RoutesStationTest {

	@Autowired
	RoutesStationLinkRepository repository;
	
	@Autowired
	BusService busService;
	
//	@Test
	public void testAdd() {
		Station station = new Station();
		station.setName("A");
		Route route = new Route();
		route.setName("R1");
		
		RouteStationLink link = new RouteStationLink();
		link.setRoute(route);
		link.setStation(station);
		
		this.repository.save(link);
		
		System.out.println("saved");
	}
	
//	@Test
	public void testRouteCreation() {
		Route route = new Route();
		route.setName("R1");
		
		Station station = new Station();
		station.setName("A");
		RouteStationLink link = new RouteStationLink();
		link.setRoute(route);
		link.setStation(station);
		this.repository.save(link);
		
		Station stationB = new Station();
		stationB.setName("B");
		link = new RouteStationLink();
		link.setRoute(route);
		link.setStation(stationB);
		this.repository.save(link);
		
		station = new Station();
		station.setName("C");
		link = new RouteStationLink();
		link.setRoute(route);
		link.setStation(station);
		this.repository.save(link);
		
		Station stationD = new Station();
		stationD.setName("D");
		link = new RouteStationLink();
		link.setRoute(route);
		link.setStation(stationD);
		this.repository.save(link);
		
		// b - d
		route = new Route();
		route.setName("R2");
		
		link = new RouteStationLink();
		link.setRoute(route);
		link.setStation(stationB);
		this.repository.save(link);
		
		link = new RouteStationLink();
		link.setRoute(route);
		link.setStation(stationD);
		this.repository.save(link);
		
		System.out.println("saved");
	}
	
	@Test
	@org.springframework.transaction.annotation.Transactional(isolation=Isolation.READ_UNCOMMITTED)
	public void testFetch() {
		try {
			this.testRouteCreation();
		} catch ( Exception e) {
			e.printStackTrace();
		}
		Station s1 = new Station();
		s1.setName("A");
		Station s2 = new Station();
		s2.setName("D");

		List<Route> routes = this.busService.getRoutesFor(s1, s2);

		for (Route route : routes) {
			System.out.println(route.getName());
		}
	}
	
}
